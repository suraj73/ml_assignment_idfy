
#importing modules.
import os
import numpy as np
import matplotlib.pyplot as plt
import random
from pathlib import Path
from collections import Counter
import argparse
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import cv2
from scipy import ndimage
import pandas as pd

num_to_char = layers.experimental.preprocessing.StringLookup(vocabulary=char_to_num.get_vocabulary(), mask_token=None, invert=True)
char_to_num = layers.experimental.preprocessing.StringLookup(vocabulary=list(characters), num_oov_indices=0, mask_token=None)

height=300
width = 100
max_length=7

#helper function
def image_label_gen(img_path, label=None):
    # 1. Read image
    img = tf.io.read_file(img_path)
    # 2. Decode and convert to grayscale
    img = tf.io.decode_png(img, channels=1)
    # 3. Convert to float32 in [0, 1] range
    img = tf.image.convert_image_dtype(img, tf.float32)
    # 4. Resize to the desired size
    img = tf.image.resize(img, [height, width])
    # 5. Transpose the image because we want the time
    # dimension to correspond to the width of the image.
    img = tf.transpose(img, perm=[1, 0, 2])
    # 6. Map the characters in label to numbers
    label = char_to_num(tf.strings.unicode_split(label, input_encoding="UTF-8"))
    # 7. Return a dict as our model is expecting two inputs
    return {"image": img, "label": label}
#CTC Layer for loading the model
class CTCLayer(layers.Layer):
    #adding extra kwargs for easier loading
    def __init__(self, name=None, **kwargs):
        super().__init__(name=name)
        self.loss_fn = keras.backend.ctc_batch_cost

    def call(self, y_true, y_pred):
        # Compute the training-time loss value and add it
        # to the layer using `self.add_loss()`.
        batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
        input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
        label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

        input_length = input_length * tf.ones(shape=(batch_len, 1), dtype="int64")
        label_length = label_length * tf.ones(shape=(batch_len, 1), dtype="int64")

        loss = self.loss_fn(y_true, y_pred, input_length, label_length)
        self.add_loss(loss)

        # At test time, just return the computed predictions
        return y_pred

# Inference pipeline
def inference_pipeline(input_image):
    input_len = np.ones(input_image.shape[0]) * input_image.shape[1]
    predictions = keras.backend.ctc_decode(input_image, input_length=input_len, greedy=True)[0][0][:, :max_length]
    # Iterate over the predictions and get back the text
    output_text = []
    for res in predictions:
        res = tf.strings.reduce_join(num_to_char(res)).numpy().decode("utf-8")
        output_text.append(res)
    return output_text

#creating a parser
parser = argparse.ArgumentParser(description= 'inferencing via command line')
parser.add_argument(
        "--weights_path",
        default="",
        type=str,
        help="Loads the weight from given path",
    )
parser.add_argument(
            "--input_image", type=str, help="path to the input image.
        )

# main code logic
#parsing the args
args = parser.parse_args()

"""# Loading the trained model."""
#loading the model with CTC layer.
inference_model = tf.keras.models.load_model(args.weights_path,custom_objects={'CTCLayer':CTCLayer})

# extracting the model from top and bottom.
prediction_model = keras.models.Model(inference_model.get_layer(name="image").input, inference_model.get_layer(name="dense2").output)

encoded_img, label = image_label_gen(args.input_image)
test_predictions = prediction_model.predict(encoded_img)
test_pred_labels = inference_pipeline(test_predictions)
print("Predicted_label is {}".format(test_pred_labels))
#reading and displaying the image
img = cv2.imread(args.input_image)
plt.imshow(img)
plt.show()
