#importing modules.
import os
import numpy as np
import matplotlib.pyplot as plt
import random
from pathlib import Path
from collections import Counter

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import cv2
from scipy import ndimage
import pandas as pd
#reading the dataframe
dataframe = pd.read_csv('ML_assignment_IDfy/dataset.csv')
dataframe
#fetching labels,images and characters
labels = dataframe['9B52145'].tolist()
images = dataframe['crop_m1/I00000.png'].tolist()
characters = set(char for label in labels for char in label)
#Debug :: Uncommment,If needed
# print("Number of images found: ", len(images))
# print("Number of labels found: ", len(labels))
# print("Number of unique characters: ", len(characters))
# print("Characters present: ", characters)

#defining few hyperparameters
batch_size = 1
width = 300
height = 100
# Maximum length of any captcha in the dataset
max_length = max([len(label) for label in labels])
#mapper functions:: Characters to number and numbers to Characters
char_to_num = layers.experimental.preprocessing.StringLookup(vocabulary=list(characters), mask_token=None)
num_to_char = layers.experimental.preprocessing.StringLookup(vocabulary=list(characters), mask_token=None,invert=True)
# 80-20 ratio as suggested as suggested in the assignment
def generate_train_val(images, labels, train_size=0.8):
    size = len(images)
    indices = np.arange(size)
    train_samples = int(size * train_size)
    #Splitting the data into train and val sets
    train_images, train_labels = images[indices[:train_samples]], labels[indices[:train_samples]]
    val_images, val_labels = images[indices[train_samples:]], labels[indices[train_samples:]]
    #returning train and val pairs
    return train_images, train_labels, val_images, val_labels


# Splitting data into training and validation sets
train_images, train_labels, val_images, val_labels = generate_train_val(np.array(images), np.array(labels))


def image_label_gen(img_path, label):
    # 1. Read image
    img = tf.io.read_file(img_path)
    # 2. Decode and convert to grayscale
    img = tf.io.decode_png(img, channels=1)
    # 3. Convert to float32 in [0, 1] range
    img = tf.image.convert_image_dtype(img, tf.float32)
    # 4. Resize to the desired size
    img = tf.image.resize(img, [height, width])
    # 5. Transpose the image because we want the time
    # dimension to correspond to the width of the image.
    img = tf.transpose(img, perm=[1, 0, 2])
    # 6. Map the characters in label to numbers
    label = char_to_num(tf.strings.unicode_split(label, input_encoding="UTF-8"))
    # 7. Return a dict as our model is expecting two inputs
    return {"image": img, "label": label}


train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_dataset = (train_dataset.map(image_label_gen).batch(batch_size).prefetch(buffer_size=tf.data.experimental.AUTOTUNE))

validation_dataset = tf.data.Dataset.from_tensor_slices((x_valid, y_valid))
validation_dataset = (validation_dataset.map(image_label_gen).batch(batch_size).prefetch(buffer_size=tf.data.experimental.AUTOTUNE))

#tweaking the CTC layer for easier loading and saving with *kwargs
class CTCLayer(layers.Layer):
    #adding extra kwargs for easier loading
    def __init__(self, name=None, **kwargs):
        super().__init__(name=name)
        self.loss_fn = keras.backend.ctc_batch_cost

    def call(self, y_true, y_pred):
        # Compute the training-time loss value and add it
        # to the layer using `self.add_loss()`.
        batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
        input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
        label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

        input_length = input_length * tf.ones(shape=(batch_len, 1), dtype="int64")
        label_length = label_length * tf.ones(shape=(batch_len, 1), dtype="int64")

        loss = self.loss_fn(y_true, y_pred, input_length, label_length)
        self.add_loss(loss)

        # At test time, just return the computed predictions
        return y_pred

#tweaking the regularisation (dropout) of the model along with increasing num_features in conv layers :: To avoid overfitting
def build_model():
    # Inputs to the model
    input_img = layers.Input(
        shape=(width, height, 1), name="image", dtype="float32"
    )
    labels = layers.Input(name="label", shape=(None,), dtype="float32")

    # First conv block
    x = layers.Conv2D(
        64,
        (3, 3),
        activation="relu",
        kernel_initializer="he_normal",
        padding="same",
        name="Conv1",
    )(input_img)
    x = layers.MaxPooling2D((2, 2), name="pool1")(x)

    # Second conv block
    x = layers.Conv2D(
        128,
        (3, 3),
        activation="relu",
        kernel_initializer="he_normal",
        padding="same",
        name="Conv2",
    )(x)
    x = layers.MaxPooling2D((2, 2), name="pool2")(x)

    new_shape = ((width // 4), (height // 4) * 128)
    x = layers.Reshape(target_shape=new_shape, name="reshape")(x)
    x = layers.Dense(64, activation="relu", name="dense1")(x)
    x = layers.Dropout(0.5)(x)

    # RNNs
    x = layers.Bidirectional(layers.LSTM(128, return_sequences=True, dropout=0.4))(x)
    x = layers.Bidirectional(layers.LSTM(64, return_sequences=True, dropout=0.4))(x)

    # Output layer
    x = layers.Dense(len(characters) + 1, activation="softmax", name="dense2")(x)

    # Add CTC layer for calculating CTC loss at each step
    output = CTCLayer(name="ctc_loss")(labels, x)

    # Define the model
    model = keras.models.Model(
        inputs=[input_img, labels], outputs=output, name="ocr_model_v1"
    )
    # Optimizer
    opt = keras.optimizers.Adam()
    # Compile the model and return
    model.compile(optimizer=opt)
    return model


# Get the model
model = build_model()
model.summary()

"""#Training the model."""

epochs = 1000

# Train the model
history = model.fit(
    train_dataset,
    validation_data=validation_dataset,
    epochs=epochs
)


"""# Saving the model."""
model.save('/content/model.h5')



