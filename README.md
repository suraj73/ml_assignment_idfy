# About
Assignment for Sr. ML Engineer Role at Idfy.

* Task instructions can be referred via ML Assignment 1 - OCR.pdf. 
* Dataset has been cached at dataset folder


#### Training
```
python train.py
# Add proper paths to the csv file and images.
```
>> * Refer notebook/train_test_ocr.ipynb for detailed visualizations. 
>> * Alternatively, Google Colab notebook can be accessed via https://colab.research.google.com/drive/1pLub3jSBZEUHXna3ps0JADS5qOzWKFEn?usp=sharing

#### Inference
```
python test.py  --weights_path model/model.h5  --input_image /path/to/cropped/image
```

#### Remarks
1. Regularisation has been added to avoid model overfitting.
2. For enhanced accuracy, consider adding more images. The current model is trained on LQ,HQ images.
3. Model has been trained for 500 Epochs.
4. Various modifications in network architecture has been tried but the current one with tweaked params gives the best result in the desired timeframe.

#### Credits
https://github.com/keras-team/keras-io/tree/master/examples/vision


